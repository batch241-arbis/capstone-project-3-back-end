const Product = require("../Models/Products");
const auth = require("../auth");

module.exports.createProduct = (reqBody) => {
	let newProduct = new Product({
		name: reqBody.name,
		image: reqBody.image,
		description: reqBody.description,
		price: reqBody.price,
		stock: reqBody.stock
	})

	return newProduct.save().then((product, error) => {
			if (error) {
				let addProductSuccess = {
					addProductSuccess: false
					}
				return addProductSuccess
			} else {
				let addProductSuccess = {
					addProductSuccess: true
					}
				return addProductSuccess
			}
	})
}

module.exports.retrieveProducts = () => {
	return Product.find().then(result => {
		return result;
	});
}

module.exports.retrieveActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
}

module.exports.retrieveSpecificProduct = (reqParams) => {
	return Product.findById(reqParams.id).then(result => {
		return result;
	});
}

module.exports.editProduct = (reqParams, reqBody) => {
	let updateProduct = {
			name: reqBody.name,
			image: reqBody.image,
			description: reqBody.description,
			price: reqBody.price,
			stock: reqBody.stock
		};
		console.log(updateProduct)
	return Product.findByIdAndUpdate(reqParams.id, updateProduct).then((result, error) => {
		if(error){
			let editProductSuccess = {
				editProductSuccess: false
			}
			return editProductSuccess;
		} else {
			let editProductSuccess = {
				editProductSuccess: true
			}
			return editProductSuccess;
		}
	});
}

module.exports.disableProduct = (reqParams) => {
	return Product.findByIdAndUpdate(reqParams.id, {isActive: false}).then(result => {
		console.log(result);
		return result;
	});
}

module.exports.enableProduct = (reqParams) => {
	return Product.findByIdAndUpdate(reqParams.id, {isActive: true}).then(result => {
		console.log(result);
		return result;
	});
}