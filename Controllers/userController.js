const User = require("../Models/Users");
const Product = require("../Models/Products");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email: reqBody.email.toLowerCase(),
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return User.find({email:reqBody.email.toLowerCase()}).then(result => {
		if(result.length > 0) {
			let registerSuccess = {
				registerSuccess: false
				}
			return registerSuccess
		} else {
			return newUser.save()
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			let loginSuccess = {
				loginSuccess: false
			}
			return loginSuccess
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				let loginSuccess = {
					loginSuccess: false
				}
				return loginSuccess
			}
		}
	})
}

module.exports.getProfile = (reqBody) => {
	return User.findOne({_id: reqBody.id}).then(result => {
		if (result == null) {
			let retrieveAccessSuccess = {
				retrieveAccessSuccess: false
				}
			return false
		} else {
			result.password = "";
			return result
		}
	})
}

module.exports.orderCheckout = async (userData, orderDetails) => {
	if (!userData.isAdmin) {
		let userOrderChanged = await User.findById(userData.id, {orders: true}).then(user => {
			user.orders.push({
				products:{
					name: orderDetails.product, 
					quantity: orderDetails.quantity
				}
			})
			return user.save().then(result => {
				return result.orders[result.orders.length-1].id
			})
		}).catch(err => console.log(err));

		let productDetailsChanged = await Product.findById(orderDetails.id.id).then(product => {
			product.stock = product.stock - orderDetails.quantity;
			product.orders.push({orderId: userOrderChanged});
			return product.save().then(result => {
				return true
			}).catch(err => console.log(err));
		})

		if (userOrderChanged && productDetailsChanged){
			let message = {message: "Order successfully added."}
			return message
		} else {
			let message ={message: "Order failed to be added."}
			return message
		}
	} else {
		let message = {message: "Admins cannot checkout orders."}
		return message;
	}
}