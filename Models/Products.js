const mongoose = require("mongoose")
const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product name required."]
	},
	description: {
		type: String,
		required: [true, "Product description required."]
	},
	price: {
		type: Number,
		required: [true, "Product price required."]
	},
	stock: {
		type: Number,
		required: [true, "Product stock required."]
	},
	image: {
		type: String,
		default: 'https://developers.elementor.com/docs/assets/img/elementor-placeholder-image.png'
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [{
		orderId: {
			type: String,
		}
	}]
})

module.exports = mongoose.model("Product", productSchema);