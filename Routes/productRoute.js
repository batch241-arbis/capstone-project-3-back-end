const express = require("express");
const router = express.Router();
const productController = require("../Controllers/productController");
const auth = require("../auth");

router.post("/create", (req, res) => {
	productController.createProduct(req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/all", (req, res) => {
	productController.retrieveProducts().then(resultFromController => res.send(resultFromController));
});

router.get("/forsale", (req, res) => {
	productController.retrieveActiveProducts().then(resultFromController => res.send(resultFromController));
});

router.get("/:id", (req, res) => {
	productController.retrieveSpecificProduct(req.params).then(resultFromController => res.send(resultFromController));
});

router.post("/edit/:id", (req, res) => {
	productController.editProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/disable/:id", (req, res) => {
	productController.disableProduct(req.params).then(resultFromController => res.send(resultFromController));
});

router.post("/enable/:id", (req, res) => {
	productController.enableProduct(req.params).then(resultFromController => res.send(resultFromController));
});

module.exports = router;