const express = require("express");
const router = express.Router();
const userController = require("../Controllers/userController");
const auth = require("../auth");

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/detail", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});

router.post("/checkout/:id", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	const orderDetails = {
		id: req.body.productId,
		quantity: req.body.quantity
	}
	userController.orderCheckout(userData, orderDetails).then(resultFromController => res.send(resultFromController))
});

module.exports = router;