const express = require(`express`);
const mongoose = require(`mongoose`);
const cors = require(`cors`);

const userRoute = require("./Routes/userRoute");
const productRoute = require("./Routes/productRoute");

const app = express();
const db = mongoose.connection;
const port = 4000;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://admin123:admin123@cluster0.eua2rv1.mongodb.net/E-Commerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

app.use("/users", userRoute);
app.use("/products", productRoute);

db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the Atlas"));

app.listen(port, () => console.log(`API is now online on port ${port}`));